#ifndef SCENARIO_H
#define SCENARIO_H

// local
class TestTorus;
class Vector;

// gmlib
namespace GMlib {

  class Scene;
  class Camera;
  class PointLight;
  class DefaultSelectRenderer;
  class DefaultRenderer;
  class RenderTarget;
  class SceneObject;
  template <typename T> class PTorus;
  template <typename T> class PSphere;
  template <typename T> class PCylinder;
  template <typename T> class PPlane;
  template <typename T, int n> class Point;

  template <typename T, int n>
  class Vector;

  class Angle;
  //template<typename T> class PTorus;
}

// qt
#include <QObject>
#include <QRect>
//qgui
#include <QKeyEvent>
// stl
#include <iostream>
#include <memory>
#include <queue>

class Scenario: public QObject {
  Q_OBJECT
public:
  explicit Scenario();
  virtual ~Scenario();

  void                                              initialize();
  void                                              deinitialize();
  virtual void                                      initializeScenario();

  void                                              startSimulation();
  void                                              stopSimulation();
  void                                              toggleSimulation();

  void                                              render( const QRect& viewport, GMlib::RenderTarget& target );
  void                                              prepare();

  void                                              replotTestTorus();


  void                                              zoomCameraW(const float &zoom_var);
GMlib::Point<int, 2>                                fromQtToGMlibViewPoint(const GMlib::Camera& cam, const QPoint& pos);
  float                                             cameraSpeedScale();
  void                                              moveCamera(const QPoint &begin_pos, const QPoint &end_pos);
  void                                              panHorizontal(const int &_delta);
  void                                              panVertical(const int &_delta);

GMlib::SceneObject*                                 findSceneObj(const QPoint &pos);
  void                                              getObj(GMlib::SceneObject *selected_obj);
  void                                              switchCam(int n);

  void                                              deselectObj();

  void                                              camFly(GMlib::Vector<float,3> dS, GMlib::Angle dA, GMlib::Vector<float,3> axis);
  void                                              camFlyUp();
  void                                              camFlyDown();
  void                                              camFlyRight();
  void                                              camFlyLeft();

  void                                              lockOnObj( GMlib::SceneObject* loking_obj);
  void                                              selectObjects(const QPoint &qpos);
  GMlib::SceneObject*                               findAllObj();

  void                                              save();
  void                                              load();

  bool                                              _doneLevelSix = false;
  bool                                              _doneLevelFive = false;
  bool                                              _doneLevelFour = false;
  bool                                              _doneLevelThree = false;
  bool                                              _doneLevelTwo = false;
  bool                                              _doneLevelOne = false;
  std::queue<std::shared_ptr<GMlib::SceneObject>>   _sceneObjectQueue;
  std::vector<double>                               _colorVector;               // Helper variable for loading in material for surfaces
  std::vector<double>                               _movementVector;            // Helper variable for moving surfaces to the position saved at
  std::vector<std::shared_ptr<GMlib::SceneObject>>  _sceneObjectKeepAliveVector;


  void                                              setSelectObj(GMlib::SceneObject *obj);

  void                                              storeObj(GMlib::SceneObject *obj);

  void                                              selectAllObj();
  void                                              selectChildrenObj(GMlib::SceneObject* object);
  void                                              deselectAllObj();
  void                                              toggleSelectAll();
  void                                              moveObj(QPoint &pos,QPoint &prev);

  void                                              testVar();

  void                                              rotateObj(QPoint &pos,QPoint &prev);
  void                                              scaleObj(int &delta);

  void                                              unlockObjs();
  void                                              insertSphere(float radius, const QPoint &pos);
  void                                              deleteObject();

  void                                              bezierForm();
  void                                              insertObject();
  void                                              changeColor();
  GMlib::SceneObject*                               _selectedObjVar=nullptr;

  void                                              replotLow();
  void                                              replotHigh();
  void                                              insertPtorus(float radius, const QPoint &pos);




protected:
  void                                              timerEvent(QTimerEvent *e) override;

private:
  std::shared_ptr<GMlib::Scene>                     _scene;
  int                                               _timer_id;

  //select_renderer
  std::shared_ptr<GMlib::DefaultSelectRenderer>     _select_renderer {nullptr};

  std::shared_ptr<GMlib::DefaultRenderer>           _renderer { nullptr };
  std::shared_ptr<GMlib::Camera>                    _camera   { nullptr };
  QRect                                             _viewport { QRect(0,0,1,1) };

  std::shared_ptr<GMlib::PointLight>                _light;
  std::shared_ptr<TestTorus>                        _testtorus;
  std::shared_ptr<TestTorus>                        _testtorus2;

private:
  void                                              save( std::ofstream& os, const GMlib::SceneObject* obj);
  void                                              saveSO( std::ofstream& os, const GMlib::SceneObject* obj);
  void                                              savePT( std::ofstream& os, const GMlib::PTorus<float>* obj);
  void                                              savePS(std::ofstream &os, const GMlib::PSphere<float> *obj);
  void                                              savePC(std::ofstream &os, const GMlib::PCylinder<float> *obj);
  void                                              savePP(std::ofstream &os, const GMlib::PPlane<float> *obj);
  static std::unique_ptr<Scenario>                  _instance;
public:
  static Scenario&                                  instance();
};

#endif
