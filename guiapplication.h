#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include "scenario.h"
#include "window.h"

// qt
#include <QGuiApplication>
//messagebox


// stl
#include <memory>
#include <queue>
namespace GMlib {
  template <typename T, int n> class Point;
}



class GuiApplication : public QGuiApplication {
  Q_OBJECT
public:
  explicit GuiApplication(int& argc, char** argv);
  ~GuiApplication();

private:
  Window                                      _window;
  Scenario                                    _scenario;
  std::queue<std::shared_ptr<QInputEvent>>    _input_events;
  bool                                        _Mouse_pressed;
  QPoint                                      _startpos{0,0};
  QPoint                                      _endpos{0,0};
  //std::unordered_map<std::string, RenderCamPair>    _rc_pairs;


private slots:
  void                                        handleMouseMove(QMouseEvent*);
  void                                        handleMouseRelease(QMouseEvent*);
  void                                        handleMousePress(QMouseEvent*);
  void                                        handleKeyPress(QKeyEvent*);
  void                                        handleMouseWheel(QWheelEvent*);

  virtual void                                onSceneGraphInitialized();
  virtual void                                afterOnSceneGraphInitialized();

  virtual void                                onSceneGraphInvalidated();
  void                                        handleGLInputEvents();
signals:

  void                                        signOnSceneGraphInitializedDone();
};

#endif // GUIAPPLICATION_H
