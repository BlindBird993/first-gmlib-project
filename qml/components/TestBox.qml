import QtQuick 2.1
//import SceneGraphRendering 1.0

Rectangle{


 ListView {
    id: clippedList
    x: 20;
    y: 20;
    width: 70;
    height: 100;
    clip: true;
    model:["A","B","C"]

    delegate: Rectangle{
    color: "lightblue"
    width: parent.width;
    height: 25;

    Text{
    anchors.fill: parent
    horizontalAlignment:Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
            }
        }
    }
 ListView{
    id:clippedDelegateList
    x:clippedList.x + clippedList.width + 20
    y:20
    width:70
    height: 100
    clip:true
    model:["A","B","C"]

    delegate: Rectangle {
    color: "lightblue"
    width: parent.width
    height: 25
    clip: true

    Text{
    text: modelData
    anchors.fill: parent
    horizontalAlignment: Text.AlignHCenter
    verticalAlignment: Text.AlignVCenter
        }
      }
    }
}
