//Сделать как FPSBox, передать в вектор значения из икс и игрек ссылками.
import QtQuick 2.1
//import SceneGraphRendering 1.0

Rectangle{

    property vector<int> coor_vec;

    color: "white";
    opacity: 0.7;

    border.color: "black";
    border.width: 2;

    Text{

    anchors.centerIn: parent;
    text: "X:" + coor_vec.front() + "Y" + coor_vec.back();
    }
    }
