import QtQuick 2.1
//import SceneGraphRendering 1.0

Rectangle{

    property int fps: 0

    color: "white";
    opacity: 0.7;

    border.color: "black";
    border.width: 2;

    Text{

    anchors.centerIn: parent;
    text: "fps:" + fps;
    }
    }
